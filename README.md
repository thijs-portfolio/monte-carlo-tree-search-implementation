# Monte Carlo Tree Search (MCTS) implementation

This project is the result of an assignment as part of the Udacity Artificial Intelligence nano degree program. Please note that only the file named **monte_carlo_agent.py** is written by me. The other files in this repository are only meant for setting up the rules of the game and running matches against my Monte Carlo agent. These files are owned by [Udacity](https://www.udacity.com/school-of-ai).

## The game

The game that is agent is playing is called "knights isolation". It is a game of isolation where the player can only make "L" shaped moves like a knight in chess. The first player that runs out of moves loses.

![Knights Isolation](https://gitlab.com/thijsbs/monte-carlo-tree-search-implementation/raw/master/images/viz.gif)

My monte carlo tree search can be tested with the following command:

    $python run_match.py -f -r 10 -o MINIMAX -t 50

This runs 10 fair matches with the MCTS agent against a MINIMAX agent with a maximum decision time of 50 miliseconds.


## The Monte Carlo Tree Search Agent

### Introduction

For the following experiment I implemented the Monte Carlo Tree Search (MCTS) game playing agent to select the best possible moves for a game of “knights isolation”. The algorithm had a certain time limit within which the best possible move should be selected for that turn. MCTS is a general algorithm and does not require game-specific information or specialized heuristics. Instead, MCTS repeatedly simulates the game outcome with random moves to obtain information about the best next move. This sampling approach is generally advantageous for large search spaces. In Monte Carlo Tree Search, a trade-off is made between exploring new paths/strategies, versus increasing confidence in previously explored paths/strategies. Tuning of the MCTS algorithm to favor exploration or exploitation is done by choosing an appropriate exploration/exploitation constant (see implementation). For this experiment, the following research question was explored:

*“Does the performance of the Monte Carlo Tree Search agent depend on the decision time, and does the agent offer an advantage for certain decision times over a baseline agent, in a game of knights isolation?”*


### Methods

The behavior of the MCTS agent was first tuned by adjusting the exploration/exploitation constant. For this the MCTS agent played 100 games against the standard MINIMAX algorithm with several different values for the exploration/exploitation constant. The constant which resulted in the best win chance was used in subsequent experiments. The MCTS agent was evaluated against a MINIMAX agent with iterative deepening. To evaluate both agents separately, both the MCTS agent and the MINIMAX agent were tested against a random agent (each 100 games with a decision time of 150 ms). To answer the research question, the MCTS agent was tested against the MINIMAX agent with a variable decision time. For each decision time, 100 games were played. . The win chance against the baseline MINIMAX agent (with iterative deepening) was recorded for different decision times. 100 games were played against the MINIMAX agent for each tested decision time. Fair matches were not enabled because the opening moves of both agents is a random position on the board.


### Results

Tuning of the MCTS agent is shown in figure 1. A exploration/exploitation constant of 2.0 was chosen the rest of the experiments. The MCTS agent won 95% of games against the random agent. The MINIMAX with iterative deepening agent won 96% of games against the random agent. Against the MINIMAX agent with iterative deepening, the MCTS agent won on between 13% and 31% of the games for different decision times. As seen in figure 2, the MCTS agent performed best for shorter decision times (peak at 50 ms), with a relatively stable 15% win chance for 100 ms and above. Unfortunately, decision times above 350 ms could not be tested because the system would freeze in between games before the 100 games could be completed.

![Knights Isolation](https://gitlab.com/thijsbs/monte-carlo-tree-search-implementation/raw/master/images/tuning.png)
*Illustration 1: Tuning of the MCTS agent*


![Knights Isolation](https://gitlab.com/thijsbs/monte-carlo-tree-search-implementation/raw/master/images/time_wins.png)
*Illustration 2: Performance of MCTS against MINIMAX (with iterative deepening), for different decisioin times*


### Discussion

Although the performance of the MCTS agent was comparable to that of the MINIMAX agent against a random agent, when competing against MINIMAX, the MCTS agent was clearly inferior in the range of tested decision times. For this variant of the game of isolation on an 9x11 board, the MINIMAX with iterative deepening agent is able to search deep enough to come up with more optimal moves than the random sampling strategy of the MCTS agent can, within the decision time. The advantage of a Monte Carlo search strategy is that it is more general and does not require a game-specific implementation or heuristic. For more complex games with high branching factors, the MCTS agent could prove to be more advantageous than MINIMAX. It has been shown that MCTS decisions converges to those of MINIMAX [1], given enough sampling time. For this reason it is a shame that longer decision times could not be compared, as the MCTS agent should in theory converge to a 50% win chance against MINIMAX, given a long enough decision time.

Since decision times above 350 ms could not be reached with the current setup, the research question of the experiment can only be partially answered: The MCTS agent performance does depend on the decision time in the range of 15-350 ms, with better performance with shorter decision times. In this range there was no decision time that gave the MCTS agent an advantage over the MINIMAX agent. However, for longer decision times, the performance of the MCTS agent could not be tested.

### References

[1] Bouzy, Bruno. “Old-Fashioned Computer Go vs Monte-Carlo Go”. IEEE Symposium on Computational Intelligence and Games. 2007

.