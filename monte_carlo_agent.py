from sample_players import DataPlayer
import random
import math
# import numpy as np

class CustomPlayer(DataPlayer):
    """ Implement your own agent to play knight's Isolation

    The get_action() method is the only required method for this project.
    You can modify the interface for get_action by adding named parameters
    with default values, but the function MUST remain compatible with the
    default interface.

    **********************************************************************
    NOTES:
    - The test cases will NOT be run on a machine with GPU access, nor be
      suitable for using any other machine learning techniques.

    - You can pass state forward to your agent on the next turn by assigning
      any pickleable object to the self.context attribute.
    **********************************************************************
    """

    class Node:
        """A node in the monte-carlo tree
        """
        __slots__ = ("state", "parent", "score", "visits", "children")

        def __init__(self, state, parent):
            self.state = state
            self.parent = parent
            self.score = 0
            self.visits = 0
            self.children = []


        def expand(self):
            """Expand the node"""
            self.children = [self.__class__(self.state.result(a), self) for a in self.state.actions()]

        def select_child(self, root):
            """Select the best child"""
            # The following line works on my machine, but numpy doesn't appear to work with the remote autograder
            # return self.children[np.argmax([c.ucb1(root) for c in self.children])]
            # Non-numpy solution:
            scores = [c.ucb1(root) for c in self.children]
            return self.children[scores.index(max(scores))]

        def ucb1(self, root):
            """return the ucb1 score of the node"""
            # import numpy as np

            if self.visits == 0:
                return float("inf")
            return self.score / self.visits + 2. * math.sqrt(math.log(root.visits) / self.visits)

        def backpropagate(self, reward):
            """Backpropagate the simulated score back up the tree"""
            node = self
            while node is not None:
                node.visits += 1
                node.score += reward
                node = node.parent

    def tree_policy(self, root_node):
        """Return the child node to simulate"""
        node = root_node
        while True:
            if node.children:
                node = node.select_child(root_node)
            else:
                if node.visits == 0:
                    return node
                else:
                    node.expand()
                    return []

    def default_policy(self, state):
        """Rollout the state to a terminal state"""
        while not state.terminal_test():
            state = state.result(random.choice(state.actions()))
        if state.utility(self.player_id) > 0:
            return 1
        elif state.utility(self.player_id) < 0:
            return -1

    def mcts(self, state):
        import random
        # Start the tree search
        root = self.Node(state, parent=None)
        iter_limit = 50
        if root.state.terminal_test():
            return random.choice(state.actions())
        for _ in range(iter_limit):
            child = self.tree_policy(root)
            if not child:
                continue
            reward = self.default_policy(child.state)
            child.backpropagate(reward)
        scores = [c.ucb1(root) for c in root.children]
        # The following line works on my machine, but numpy doesn't appear to work with the remote autograder
        # return state.actions()[np.argmax([c.ucb1(root) for c in root.children])]
        # Non-numpy solution:
        return state.actions()[scores.index(max(scores))]


    def get_action(self, state):
        if state.ply_count < 2:
            self.queue.put(random.choice(state.actions()))
        else:
            self.queue.put(self.mcts(state))
